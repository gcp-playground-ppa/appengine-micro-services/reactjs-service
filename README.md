# Reactjs Service

simple ui main service made with Reactjs

# Use Locally

Use this as any ReactJs App. `npm run start` 

# Deploy to Google Cloud App Engine

1.  Compile : `npm run build`
2.  Connect to your GCP account, verify that API Cloud Compile is enabled.
4.  Deploy to GCP : `gcloud app deploy`

# Eléments de context

Cette application permet d'accéder rapidement à AppEngine Standard 
car ReactJs est mis en mémoire plus rapidement qu'une app Java.

Todo : 
* Mise en place lecteur de code barre : https://www.npmjs.com/package/javascript-barcode-reader
* Mise en place d'un cookie pour le token Bearer. Actuellement, 
F5 cause la perte du cookie et il faut systématiquement passer par "/"...
* Mise en place internationalisation (react-i18next)


