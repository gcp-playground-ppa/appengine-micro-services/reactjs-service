import React from 'react';
import { EnzymePropSelector, shallow, ShallowWrapper } from 'enzyme';
import App, { AppComponent } from "./App";
import MicroservicesLoadingView from "./component/MicroservicesLoadingView";
import { mapStateToProps } from "./App.redux";
import { RootState } from "./redux/store/index";
import { initialServicesState } from "./redux/types/ServicesState";
import WelcomeView from "./component/WelcomeView";

let bundle: { wrapper: ShallowWrapper<typeof App>, instance: typeof App };
const wakeAction = jest.fn()

beforeEach(() => {
    wakeAction.mockClear();
});

describe("When App start", () => {

    let useEffect: jest.SpyInstance<void, [React.EffectCallback, (ReadonlyArray<any> | undefined)?]>;

    const mockUseEffect = () => {
      useEffect.mockImplementationOnce(f => f());
    };

    beforeEach(() => {
        useEffect = jest.spyOn(React, "useEffect");
        mockUseEffect();
    });

    it("Welcome View should be displayed", () => {
        givenApp();

        thenShouldDisplay(WelcomeView);
    });

    it("should wake services", () => {
        givenApp();

        expect(wakeAction).toHaveBeenCalledWith("Bibliothèque Dvds");
    });
});

function givenApp() {
    let wrapper = shallow(<AppComponent wake={wakeAction} />);
    let instance = wrapper.instance() as unknown as typeof App;
    bundle = { wrapper, instance };
}

function thenShouldDisplay(Loadings: EnzymePropSelector) {
    expect(bundle.wrapper.find(Loadings).length).not.toBe(0);
}

