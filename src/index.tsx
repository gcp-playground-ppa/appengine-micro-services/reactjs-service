import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Login from './pages/Login';
import {Provider} from "react-redux";
import Store from "./redux/Store";
import * as serviceWorker from './serviceWorker';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import DvdAdd from "./pages/DvdAdd";
import Services from './pages/Services';
import DvdRouter from "./router/DvdRouter";

ReactDOM.render(
    <Provider store={Store}>
        <Router>
            <Switch>
                <Route exact={true} path="/login" render={(props) => <Login {...props}/>}/>
                <Route exact={true} path="/add" render={(props) => <DvdAdd/>}/>
                <Route exact={true} path="/services" render={(props) => <Services/>}/>
                <Route path="/dvd/" render={(props) => <DvdRouter {...props}/>}/>
                <Route path="/" render={(props) => <App {...props}/>}/>
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
