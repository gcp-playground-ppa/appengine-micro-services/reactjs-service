import {initialServicesState, IServiceState, servicesReducer} from "../ServicesState";
import {fetchServiceFail, fetchServicePending, fetchServiceSuccess, setBearer} from "../../actions/AppActions";

let state: IServiceState;
let newState: typeof state;

describe("Services Reducer  ", () => {

    it("can reduce action wake", () => {
        initialiseState({stateNbr: 1});
        newState = servicesReducer(state, fetchServicePending("toto"));

        const service = newState.all.get("toto");
        if (service) {
            expect(service.url).toEqual("http");
            expect(service.state).toEqual(0);
        } else {
            fail();
        }
    });

    it("can reduce action wake unknown service and no change", () => {
        initialiseState({stateNbr: 1});
        newState = servicesReducer(state, fetchServicePending("titi"));

        const service = newState.all.get("toto");
        if (service) {
            expect(service.url).toEqual("http");
            expect(service.state).toEqual(1);
        } else {
            fail();
        }
    });

    it("can reduce action wake success", () => {
        initialiseState();
        newState = servicesReducer(state, fetchServiceSuccess("toto"));

        const service = newState.all.get("toto");
        if (service) {
            expect(service.url).toEqual("http");
            expect(service.state).toEqual(1);
        } else {
            fail();
        }
    });

    it("can reduce action wake success on unknown service, and no change", () => {
        initialiseState();
        newState = servicesReducer(state, fetchServiceSuccess("titi"));

        const service = newState.all.get("toto");
        if (service) {
            expect(service.url).toEqual("http");
            expect(service.state).toEqual(0);
        } else {
            fail();
        }
    });

    it("can reduce action wake fail", () => {
        initialiseState();
        newState = servicesReducer(state, fetchServiceFail("toto"));

        const service = newState.all.get("toto");
        if (service) {
            expect(service.url).toEqual("http");
            expect(service.state).toEqual(-1);
        } else {
            fail();
        }
    });

    it("can reduce action wake fail on unknown service, and no change", () => {
        initialiseState();
        newState = servicesReducer(state, fetchServiceFail("titi"));

        const service = newState.all.get("toto");
        if (service) {
            expect(service.url).toEqual("http");
            expect(service.state).toEqual(0);
        } else {
            fail();
        }
    });

    it("can reduce action set Bearer", () => {
        initialiseState();
        newState = servicesReducer(state, setBearer("toto"));

        expect(newState.bearer).toEqual("toto");
    });
});

function initialiseState({name = "toto", url = "http", stateNbr = 0, bearer = ""} = {}) {
    const all: Map<string, { url: string, state: number }> = new Map();
    all.set(name, {url: url, state: stateNbr});
    state = {
        ...initialServicesState,
        all,
        bearer,
    };
}
