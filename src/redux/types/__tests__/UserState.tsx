import {initialServicesState, IUserState, userStateReducer} from "../UserState";
import {
    addDvdFail,
    addDvdPending,
    addDvdSuccess,
    deleteDvdFail,
    deleteDvdPending,
    deleteDvdSuccess,
    displayModal,
    fetchDvdsFail,
    fetchDvdsPending,
    fetchDvdsSuccess,
    fetchEanFail, fetchEanPending,
    hideModal,
    setError
} from "../../actions/AppActions";
import {Dvd} from "../../dto/Dvd";

let state: IUserState;
let newState: typeof state;

describe("User Reducer add DVD", () => {

    it("can reduce action pending", () => {
        initialiseState();
        newState = userStateReducer(state, addDvdPending());
        expect(newState.waiting).toEqual(true);
    });

    it("can reduce action Fail", () => {
        initialiseState({waiting: true});
        newState = userStateReducer(state, addDvdFail("somme Error"));
        expect(newState.error).toEqual("somme Error");
        expect(newState.waiting).toEqual(false);
    });

    it("can reduce action Success", () => {
        initialiseState({waiting: true});
        newState = userStateReducer(state, addDvdSuccess([
            new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
            new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
        ]));
        expect(newState.dvds).toEqual([
            new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
            new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
        ]);
        expect(newState.waiting).toEqual(false);
    });
});

describe("User Reducer fetch DVD", () => {

    it("can reduce action pending", () => {
        initialiseState();
        newState = userStateReducer(state, fetchDvdsPending());
        expect(newState.waiting).toEqual(true);
    });

    it("can reduce action Fail", () => {
        initialiseState({waiting: true});
        newState = userStateReducer(state, fetchDvdsFail("somme Error"));
        expect(newState.error).toEqual("somme Error");
        expect(newState.waiting).toEqual(false);
    });

    it("can reduce action Success", () => {
        initialiseState({waiting: true});
        newState = userStateReducer(state, fetchDvdsSuccess([
            new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
            new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
        ]));
        expect(newState.dvds).toEqual([
            new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
            new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
        ]);
        expect(newState.waiting).toEqual(false);
    });
});

describe("User Reducer delete DVD", () => {

    it("can reduce action pending", () => {
        initialiseState();
        newState = userStateReducer(state, deleteDvdPending());
        expect(newState.waiting).toEqual(true);
    });

    it("can reduce action Fail", () => {
        initialiseState({waiting: true});
        newState = userStateReducer(state, deleteDvdFail("somme Error"));
        expect(newState.error).toEqual("somme Error");
        expect(newState.waiting).toEqual(false);
    });

    it("can reduce action Success", () => {
        initialiseState({waiting: true});
        newState = userStateReducer(state, deleteDvdSuccess([
            new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
            new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
        ]));
        expect(newState.dvds).toEqual([
            new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
            new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
        ]);
        expect(newState.waiting).toEqual(false);
    });
});

describe("User Reducer search EAN", () => {

    it("can reduce action Fail", () => {
        initialiseState();
        newState = userStateReducer(state, fetchEanFail("somme Error"));
        expect(newState.error).toEqual("somme Error");
    });

    it("can reduce action set error", () => {
        initialiseState();
        newState = userStateReducer(state, setError("somme Error"));
        expect(newState.error).toEqual("somme Error");
    });
});

describe("User Reducer Modal Barcode scanner", () => {

    it("can reduce action display", () => {
        initialiseState();
        newState = userStateReducer(state, displayModal("scanner"));
        expect(newState.modal).toEqual({name: "scanner", opened: true});
    });

    it("can reduce action hide", () => {
        initialiseState();
        newState = userStateReducer(state, hideModal("scanner"));
        expect(newState.modal).toEqual({name: "scanner", opened: false});
    });

    it("can reduce action display", () => {
        initialiseState({modal: {name: "scanner", opened: true}});
        newState = userStateReducer(state, fetchEanPending("123"));
        expect(newState.modal).toEqual({name: "", opened: false});
    });
});

function initialiseState({error = "", waiting = false, modal = {name: "", opened: false}} = {}) {
    state = {
        ...initialServicesState,
        error,
        waiting,
        modal,
    };
}