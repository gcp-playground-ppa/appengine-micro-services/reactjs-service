import {eanStateReducer, IEanState, initialServicesState} from "../EanState";
import {Dvd} from "../../dto/Dvd";
import {addDvdPending, cancelAdd, fetchEanPending, fetchEanSuccess, setEAN} from "../../actions/AppActions";

let state: IEanState;
let newState: typeof state;

describe("Ean Reducer  ", () => {

    it("can reduce action fetch Ean", () => {
        initialiseState({dvds: [new Dvd("123")]});
        newState = eanStateReducer(state, fetchEanPending("456"));
        expect(newState.results).toEqual([]);
        expect(newState.ean).toEqual("456");
    });

    it("can reduce action fetch Ean success", () => {
        initialiseState();
        newState = eanStateReducer(state, fetchEanSuccess([new Dvd("123")]));
        expect(newState.results).toEqual([new Dvd("123")]);
    });

    it("can reduce action cancel add dvd", () => {
        initialiseState({dvds: [new Dvd("123")], ean: "123"});
        newState = eanStateReducer(state, cancelAdd());
        expect(newState.results).toEqual([]);
        expect(newState.ean).toEqual("");
    });

    it("can reduce action add Dvd Pending", () => {
        initialiseState({dvds: [new Dvd("123")], ean: "123"});
        newState = eanStateReducer(state, addDvdPending());
        expect(newState.results).toEqual([]);
        expect(newState.ean).toEqual("");
    });

    it("can reduce action fetch Ean Set", () => {
        initialiseState();
        newState = eanStateReducer(state, setEAN("123"));
        expect(newState.ean).toEqual("123");
    });
});

function initialiseState({dvds = [] as unknown as Dvd[], ean = ""} = {}) {
    state = {
        ...initialServicesState,
        results: dvds,
        ean: ean,
    };
}
