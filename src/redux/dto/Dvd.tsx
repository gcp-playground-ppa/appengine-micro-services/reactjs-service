// @ts-ignore
export class Dvd {
    public dvdfr: number;
    public ean: string;
    public title: string;
    public year: number;
    public cover: string;

    constructor(ean: string, title = "", year = 1970, cover = "", dvdfr = -1) {
        this.ean = ean;
        this.title = title;
        this.year = year;
        this.cover = cover;
        this.dvdfr = dvdfr;
    }
}