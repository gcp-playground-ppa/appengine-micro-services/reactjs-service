import {ThunkAction, ThunkDispatch} from "redux-thunk";
import {RootState} from "../store/index";
import {Action} from "redux";
import {fetchDvdsFail, fetchDvdsPending, fetchDvdsSuccess} from "./AppActions";

type ThunkResult<R> = ThunkAction<R, RootState, undefined, Action>;

function fetchDvd(): ThunkResult<void> {

    return (dispatch: ThunkDispatch<RootState, undefined, Action>, getState: () => RootState) => {
        const bearer = getState().services.bearer;
        const options = {
            headers: new Headers({
                "Authorization": `Bearer ${bearer}`,
                "Content-Type": "application/json;charset=UTF-8",
            }),
            method: "get",
        };

        dispatch(fetchDvdsPending());
        fetch(`${process.env.REACT_APP_SERVICE_DVDSTORE}dvd/`, options)
            .then((response) => {
                if (response.status != 200) {
                    return Promise.reject<string>(`fail to add DVD, status ${response.status}`);
                } else {
                    return response.json();
                }
            })
            .then((dvds) => {
                dispatch(fetchDvdsSuccess(dvds));
            }, (reason) => {
                dispatch(fetchDvdsFail(reason));
            });
    };
}

export default fetchDvd;