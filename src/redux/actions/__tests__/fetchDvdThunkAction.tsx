import fetchDvd from "../fetchDvdThunkAction";
import {Dvd} from "../../dto/Dvd";
import {initialServicesState} from "../../types/ServicesState";
import {fetchDvdsFail, fetchDvdsPending, fetchDvdsSuccess} from "../AppActions";

const dispatch = jest.fn();
let getState = jest.fn().mockImplementation(() => ({
    services: {
        ...initialServicesState,
        bearer: "AAAA-BB-CC-DDDD",
    },
}));

beforeEach(() => {
    dispatch.mockClear();
});

describe("addDvd action", () => {

    it("should fetch DVD API", (done) => {
        givenFetchStatus(200, [new Dvd("3344428015602")]);

        fetchDvd()(dispatch, getState, undefined);

        thenExpectFetchCalled();

        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(fetchDvdsSuccess([new Dvd("3344428015602")]));
            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("should fetch add DVD API", (done) => {
        givenFetchStatus(500, [new Dvd("3344428015602")]);

        fetchDvd()(dispatch, getState, undefined);

        thenExpectFetchCalled();

        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(fetchDvdsFail("fail to add DVD, status 500"));
            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });
});

function givenFetchStatus(status: number, json: any) {
    const mockJsonPromise = Promise.resolve(json);
    const mockFetchPromise = Promise.resolve({json: () => mockJsonPromise, status, text: jest.fn()});
    // @ts-ignore
    jest.spyOn(global, "fetch").mockImplementationOnce(() => mockFetchPromise);
}

function thenExpectFetchCalled() {
    expect(dispatch).toHaveBeenCalledWith(fetchDvdsPending());
    // @ts-ignore
    expect(global.fetch).toHaveBeenCalledWith("http://test.unit/adddvd/dvd/", {
        headers: new Headers({
            "Authorization": "Bearer AAAA-BB-CC-DDDD",
            "Content-Type": "application/json;charset=UTF-8",
        }),
        method: "get",
    });
}
