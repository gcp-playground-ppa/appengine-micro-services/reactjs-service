import searchDvd from "../searchDvdThunkAction";
import {Dvd} from "../../dto/Dvd";
import {initialServicesState} from "../../types/ServicesState";
import {fetchEanPending, fetchEanSuccess} from "../AppActions";

const dispatch = jest.fn();
let getState = jest.fn().mockImplementation(() => ({
    services: {
        ...initialServicesState,
        bearer: "AAAA-BB-CC-DDDD",
    },
}));

beforeEach(() => {
    dispatch.mockClear();
});

describe("searchDvd action", () => {

    it("should fetch dvds from EAN", (done) => {
        givenFetchStatus(200, [new Dvd("3344428015602")]);

        searchDvd("3344428015602")(dispatch, getState, undefined);

        thenExpectFetchCalledWith("3344428015602");

        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(fetchEanSuccess([new Dvd("3344428015602")]));
            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });
});

function givenFetchStatus(status: number, json: any) {
    const mockJsonPromise = Promise.resolve(json);
    const mockFetchPromise = Promise.resolve({json: () => mockJsonPromise, status, text: jest.fn()});
    // @ts-ignore
    jest.spyOn(global, "fetch").mockImplementationOnce(() => mockFetchPromise);
}

function thenExpectFetchCalledWith(expectedEan: string) {
    expect(dispatch).toHaveBeenCalledWith(fetchEanPending(expectedEan));
    // @ts-ignore
    expect(global.fetch).toHaveBeenCalledWith("http://test.unit/ean/?ean=" + expectedEan, {
        headers: new Headers({
            "Content-Type": "application/json;charset=UTF-8",
        }),
        method: "get",
    });
}
