import {Action} from "redux";
import {Dvd} from "../../dto/Dvd";
import {
    addDvdFail,
    addDvdPending,
    addDvdSuccess,
    cancelAdd,
    deleteDvdFail,
    deleteDvdPending,
    deleteDvdSuccess, displayModal,
    fetchDvdsFail,
    fetchDvdsPending,
    fetchDvdsSuccess,
    fetchEanFail,
    fetchEanPending,
    fetchEanSuccess,
    fetchServiceFail,
    fetchServicePending,
    fetchServiceSuccess, hideModal,
    setBearer,
    setEAN,
    setError,
} from "../AppActions";

describe("Actions wake services", () => {

    it("should contains wake service pending", () => {
        const beaconsFetchAction: Action = fetchServicePending("serviceName");
        expect(beaconsFetchAction).toEqual({type: "SERVICE_WAKE", data: "serviceName"});
    });

    it("should contains wake service success", () => {
        const beaconsFetchAction: Action = fetchServiceSuccess("serviceName");
        expect(beaconsFetchAction).toEqual({type: "SERVICE_WAKE_SUCCESS", data: "serviceName"});
    });

    it("should contains wake service fail", () => {
        const beaconsFetchAction: Action = fetchServiceFail("serviceName");
        expect(beaconsFetchAction).toEqual({type: "SERVICE_WAKE_ERROR", data: "serviceName"});
    });
});

describe("Actions authentication", () => {

    it("should contains setBearer action", () => {
        const setBearerAction: Action = setBearer("AA-BB-CC");
        expect(setBearerAction).toEqual({type: "SET_BEARER", data: "AA-BB-CC"});
    });

    it("should contains setError action", () => {
        const setErrorAction: Action = setError("zeError");
        expect(setErrorAction).toEqual({type: "SET_ERROR", data: "zeError"});
    });
});

describe("Actions about EAN search", () => {

    it("should contains setEan action", () => {
        const setEANAction: Action = setEAN("123");
        expect(setEANAction).toEqual({type: "EAN_SET", data: "123"});
    });

    it("should contains fetch EAN pending", () => {
        const fetchEanPendingAction: Action = fetchEanPending("123");
        expect(fetchEanPendingAction).toEqual({type: "EAN_FETCH", data: "123"});
    });

    it("should contains fetch EAN success", () => {
        const fetchEanSuccessAction: Action = fetchEanSuccess([new Dvd("123", "Zorro", 1981, "http://url/picture.com")]);
        expect(fetchEanSuccessAction).toEqual({
            type: "EAN_FETCH_SUCCESS",
            data: [new Dvd("123", "Zorro", 1981, "http://url/picture.com")]
        });
    });

    it("should contains fetch EAN fail", () => {
        const fetchEanFailAction: Action = fetchEanFail("someError");
        expect(fetchEanFailAction).toEqual({type: "EAN_FETCH_ERROR", data: "someError"});
    });
});

describe("Actions about Dvd Add", () => {

    it("should contains Add DVD pending", () => {
        const addDvdPendingAction: Action = addDvdPending();
        expect(addDvdPendingAction).toEqual({type: "DVD_ADD"});
    });

    it("should contains Add DVD success", () => {
        const addDvdSuccessAction: Action = addDvdSuccess([
            new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
            new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
        ]);
        expect(addDvdSuccessAction).toEqual({
            type: "DVD_ADD_SUCCESS",
            data: [
                new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
                new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
            ]
        });
    });

    it("should contains Add DVD fail", () => {
        const addDvdFailAction: Action = addDvdFail("someError");
        expect(addDvdFailAction).toEqual({type: "DVD_ADD_ERROR", data: "someError"});
    });

    it("should contains cancelAdd action", () => {
        const cancelAddAction: Action = cancelAdd();
        expect(cancelAddAction).toEqual({type: "EAN_CANCEL"});
    });
});

describe("Actions about Dvds list fetch", () => {

    it("should contains fetch DVDs pending", () => {
        const fetchDvdsPendingAction: Action = fetchDvdsPending();
        expect(fetchDvdsPendingAction).toEqual({type: "DVD_FETCH"});
    });

    it("should contains fetch DVDs success", () => {
        const fetchDvdsSuccessAction: Action = fetchDvdsSuccess([
            new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
            new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
        ]);
        expect(fetchDvdsSuccessAction).toEqual({
            type: "DVD_FETCH_SUCCESS",
            data: [
                new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
                new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
            ]
        });
    });

    it("should contains fetch DVDs fail", () => {
        const fetchDvdsFailAction: Action = fetchDvdsFail("someError");
        expect(fetchDvdsFailAction).toEqual({type: "DVD_FETCH_ERROR", data: "someError"});
    });
});

describe("Actions about Dvd delete", () => {

    it("should contains delete DVD pending", () => {
        const deleteDvdPendingAction: Action = deleteDvdPending();
        expect(deleteDvdPendingAction).toEqual({type: "DVD_DELETE"});
    });

    it("should contains delete DVD success", () => {
        const deleteDvdSuccessAction: Action = deleteDvdSuccess([
            new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
            new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
        ]);
        expect(deleteDvdSuccessAction).toEqual({
            type: "DVD_DELETE_SUCCESS",
            data: [
                new Dvd("123", "Zorro", 1981, "http://url/picture.com"),
                new Dvd("456", "New DVD", 1981, "http://url/picture.com/2")
            ]
        });
    });

    it("should contains delete DVD fail", () => {
        const deleteDvdFailAction: Action = deleteDvdFail("someError");
        expect(deleteDvdFailAction).toEqual({type: "DVD_DELETE_ERROR", data: "someError"});
    });
});


describe("Actions about Modal scanner", () => {

    it("should contains display scanner modal", () => {
        const displayModalAction: Action = displayModal("scanner");
        expect(displayModalAction).toEqual({type: "MODAL_SHOW", data: "scanner"});
    });

    it("should contains hide scanner modal", () => {
        const hideModalAction: Action = hideModal("scanner");
        expect(hideModalAction).toEqual({type: "MODAL_HIDE", data: "scanner"});
    });
});
