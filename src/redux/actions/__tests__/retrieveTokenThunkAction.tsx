import retrieveToken from "../retrieveTokenThunkAction";
import {setBearer} from "../AppActions";

const dispatch = jest.fn();
const getState = jest.fn()
const params = new Map<string, string>();

beforeEach(() => {
    dispatch.mockClear();

    params.clear();
    params.set('grant_type', 'authorization_code');
    params.set('client_id', "newClient");
    params.set('client_secret', 'newClientSecret');
    params.set('redirect_uri', 'http://site.url/login/');
    params.set('code', "code");
});

describe("retrieveToken action", () => {

    it("should fetch bearer from code", (done) => {
        givenFetchStatus(200, JSON.parse("{\"access_token\": \"AA-BB-CC\",\"token_type\": \"bearer\",\"expires_in\": 42980,\"scope\": \"write\"}"));

        retrieveToken("code")(dispatch, getState, undefined);

        thenExpectFetchCalledWith({
            url: "http://test.unit/oauth/token",
            method: "post",
            params
        });

        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(setBearer("AA-BB-CC"));

            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("can handle 404 error and reset bearer code", (done) => {
        givenFetchStatus(404, "");

        retrieveToken("code")(dispatch, getState, undefined);

        thenExpectFetchCalledWith({
            url: "http://test.unit/oauth/token",
            method: "post",
            params
        });

        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(setBearer(""));

            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });
});

function givenFetchStatus(status: number, json: any) {
    const mockJsonPromise = Promise.resolve(json);
    const mockFetchPromise = Promise.resolve({json: () => mockJsonPromise, status, text: jest.fn()});
    // @ts-ignore
    jest.spyOn(global, "fetch").mockImplementationOnce(() => mockFetchPromise);
}

function thenExpectFetchCalledWith(expected: { url: string, method: string, params: Map<string, string> }) {
    let stringParams = "";
    expected.params.forEach((value: string, key: string) => {
        stringParams += `${key}=${value}&`;
    });
    stringParams = stringParams.substring(0, stringParams.length - 1);
    // @ts-ignore
    expect(global.fetch).toHaveBeenCalledWith(expected.url, {
        body: stringParams,
        headers: new Headers({
            "authorization": "Basic bmV3Q2xpZW50Om5ld0NsaWVudFNlY3JldA==",
            "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
        }),
        method: expected.method,
    });
}
