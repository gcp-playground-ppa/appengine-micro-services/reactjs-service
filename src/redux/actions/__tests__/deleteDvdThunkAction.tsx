import deleteDvd from "../deleteDvdThunkAction";
import {Dvd} from "../../dto/Dvd";
import {initialServicesState} from "../../types/ServicesState";
import {deleteDvdFail, deleteDvdPending, deleteDvdSuccess} from "../AppActions";

const dispatch = jest.fn();
let getState = jest.fn().mockImplementation(() => ({
    services: {
        ...initialServicesState,
        bearer: "AAAA-BB-CC-DDDD",
    },
}));

beforeEach(() => {
    dispatch.mockClear();
});

describe("addDvd action", () => {

    it("should fetch DVD API", (done) => {
        givenFetchStatus(200, [new Dvd("3344428015602")]);

        deleteDvd(new Dvd("3344428015602"))(dispatch, getState, undefined);

        thenExpectFetchCalledWith(JSON.stringify(new Dvd("3344428015602")));

        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(deleteDvdSuccess([new Dvd("3344428015602")]));
            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("should fetch add DVD API", (done) => {
        givenFetchStatus(500, [new Dvd("3344428015602")]);

        deleteDvd(new Dvd("3344428015602"))(dispatch, getState, undefined);

        thenExpectFetchCalledWith(JSON.stringify(new Dvd("3344428015602")));

        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(deleteDvdFail("fail to add DVD, status 500"));
            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });
});

function givenFetchStatus(status: number, json: any) {
    const mockJsonPromise = Promise.resolve(json);
    const mockFetchPromise = Promise.resolve({json: () => mockJsonPromise, status, text: jest.fn()});
    // @ts-ignore
    jest.spyOn(global, "fetch").mockImplementationOnce(() => mockFetchPromise);
}

function thenExpectFetchCalledWith(expectedJson: string) {
    expect(dispatch).toHaveBeenCalledWith(deleteDvdPending());
    // @ts-ignore
    expect(global.fetch).toHaveBeenCalledWith("http://test.unit/adddvd/dvd/", {
        body: expectedJson,
        headers: new Headers({
            "Authorization": "Bearer AAAA-BB-CC-DDDD",
            "Content-Type": "application/json;charset=UTF-8",
        }),
        method: "delete",
    });
}
