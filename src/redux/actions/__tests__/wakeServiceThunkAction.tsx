import wakeService from "../wakeServiceThunkAction";
import {fetchServiceFail, fetchServicePending, fetchServiceSuccess} from "../AppActions";
import {initialServicesState} from "../../types/ServicesState";

let dispatch = jest.fn();
let getState = jest.fn().mockImplementation(() => ({
    services: initialServicesState,
}));
const service = "Bibliothèque Dvds";

beforeEach(() => {
    dispatch.mockClear();
});

describe("Wake service action", () => {

    it("should dispatch success when server is loaded", (done) => {
        jest.useFakeTimers();
        givenFetchStatus(200, JSON.parse("{\"status\":\"UP\",\"components\":{\"keycloak\":{\"status\":\"UP\"},\"ping\":{\"status\":\"UP\"}}}"));

        whenWakeUpService(service);

        thenExpectFetchCalledWith({url: "http://test.unit/adddvd/dvd/actuator/health", method: "get"});
        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(fetchServiceSuccess(service));
            expect(dispatch).not.toHaveBeenCalledWith(fetchServiceFail(service));

            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("should redo call when Fetch server is loading", (done) => {
        jest.useFakeTimers();
        givenFetchStatus(200, JSON.parse("{\"status\":\"UP\",\"components\":{\"keycloak\":{\"status\":\"LOADING\"},\"ping\":{\"status\":\"UP\"}}}"));

        whenWakeUpService(service);

        thenExpectFetchCalledWith({url: "http://test.unit/adddvd/dvd/actuator/health", method: "get"});

        thenExpectNoMoreDispatch();

        process.nextTick(() => {
            jest.runAllTimers();
            thenExpectFetchCalledWith({url: "http://test.unit/adddvd/dvd/actuator/health", method: "get"});

            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("should redo call when wrong response", (done) => {
        givenFetchStatus(200, JSON.parse("{\"missing\":\"DOWN\"}"));

        whenWakeUpService(service);

        thenExpectFetchCalledWith({url: "http://test.unit/adddvd/dvd/actuator/health", method: "get"});

        thenExpectNoMoreDispatch();

        process.nextTick(() => {
            thenExpectFetchCalledWith({url: "http://test.unit/adddvd/dvd/actuator/health", method: "get"});

            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("should redo call when Fetch Rate exceeded", (done) => {
        givenFetchStatus(200, "Rate exceeded");

        whenWakeUpService(service);

        thenExpectFetchCalledWith({url: "http://test.unit/adddvd/dvd/actuator/health", method: "get"});

        thenExpectNoMoreDispatch();

        process.nextTick(() => {
            thenExpectFetchCalledWith({url: "http://test.unit/adddvd/dvd/actuator/health", method: "get"});

            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("should dispatch success when Fetch status is UP", (done) => {
        givenFetchStatus(200, JSON.parse("{\"status\":\"UP\"}"));

        whenWakeUpService(service);

        thenExpectFetchCalledWith({url: "http://test.unit/adddvd/dvd/actuator/health", method: "get"});
        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(fetchServiceSuccess(service));
            expect(dispatch).not.toHaveBeenCalledWith(fetchServiceFail(service));

            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("should dispatch Fail when Fetch status is not UP", (done) => {
        givenFetchStatus(200, JSON.parse("{\"status\":\"DOWN\"}"));

        whenWakeUpService(service);

        thenExpectFetchCalledWith({url: "http://test.unit/adddvd/dvd/actuator/health", method: "get"});
        process.nextTick(() => {
            expect(dispatch).not.toHaveBeenCalledWith(fetchServiceSuccess(service));
            expect(dispatch).toHaveBeenCalledWith(fetchServiceFail(service));

            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("should dispatch Fail when Fetch response state is not 200", (done) => {
        givenFetchStatus(404, JSON.parse("{\"status\":\"DOWN\"}"));

        whenWakeUpService(service);

        thenExpectFetchCalledWith({url: "http://test.unit/adddvd/dvd/actuator/health", method: "get"});
        process.nextTick(() => {
            expect(dispatch).not.toHaveBeenCalledWith(fetchServiceSuccess(service));
            expect(dispatch).toHaveBeenCalledWith(fetchServiceFail(service));

            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("should dispatch Fail when service is unknown", () => {
        givenFetchStatus(404, JSON.parse("{\"status\":\"DOWN\"}"));

        whenWakeUpService("unknown");

        expect(dispatch).not.toHaveBeenCalledWith(fetchServicePending("unknown"));
        expect(dispatch).not.toHaveBeenCalledWith(fetchServiceSuccess("unknown"));
        expect(dispatch).toHaveBeenCalledWith(fetchServiceFail("unknown"));
    });
});

function whenWakeUpService(serviceName: string) {
    wakeService(serviceName)(dispatch, getState, undefined);
}

function givenFetchStatus(status: number, json: any) {
    const mockJsonPromise = Promise.resolve(json);
    const mockFetchPromise = Promise.resolve({json: () => mockJsonPromise, status, text: jest.fn()});
    // @ts-ignore
    jest.spyOn(global, "fetch").mockImplementationOnce(() => mockFetchPromise);
}

function thenExpectFetchCalledWith(expected: { url: string; method: string }) {
    expect(dispatch).toHaveBeenCalledWith(fetchServicePending(service));
    // @ts-ignore
    expect(global.fetch).toHaveBeenCalledWith(expected.url, {
        headers: new Headers({
            "Content-Type": "application/json;charset=UTF-8",
        }),
        method: expected.method,
    });
}

function thenExpectNoMoreDispatch() {
    expect(dispatch).not.toHaveBeenCalledWith(fetchServiceSuccess(service));
    expect(dispatch).not.toHaveBeenCalledWith(fetchServiceFail(service));
    dispatch.mockClear();
    // @ts-ignore
    global.fetch.mockClear();
}
