import addDvd from "../AddDvdThunkAction";
import {Dvd} from "../../dto/Dvd";
import {initialServicesState} from "../../types/ServicesState";
import {addDvdFail, addDvdPending, addDvdSuccess} from "../AppActions";

const dispatch = jest.fn();
let getState = jest.fn().mockImplementation(() => ({
    services: {
        ...initialServicesState,
        bearer: "AAAA-BB-CC-DDDD",
    },
}));

beforeEach(() => {
    dispatch.mockClear();
});

describe("addDvd action", () => {

    it("should fetch add DVD API", (done) => {
        givenFetchStatus(201, [new Dvd("3344428015602")]);

        addDvd(new Dvd("3344428015602"))(dispatch, getState, undefined);

        thenExpectFetchCalledWith(JSON.stringify(new Dvd("3344428015602")));

        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(addDvdSuccess([new Dvd("3344428015602")]));
            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("should dispatch error 409", (done) => {
        givenFetchStatus(409, [new Dvd("3344428015602")]);

        addDvd(new Dvd("3344428015602"))(dispatch, getState, undefined);

        thenExpectFetchCalledWith(JSON.stringify(new Dvd("3344428015602")));

        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(addDvdFail("Dvd déjà ajouté"));
            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });

    it("should dispatch error 500", (done) => {
        givenFetchStatus(500, [new Dvd("3344428015602")]);

        addDvd(new Dvd("3344428015602"))(dispatch, getState, undefined);

        thenExpectFetchCalledWith(JSON.stringify(new Dvd("3344428015602")));

        process.nextTick(() => {
            expect(dispatch).toHaveBeenCalledWith(addDvdFail("fail to add DVD, status 500"));
            // @ts-ignore
            global.fetch.mockRestore();
            done();
        });
    });
});

function givenFetchStatus(status: number, json: any) {
    const mockJsonPromise = Promise.resolve(json);
    const mockFetchPromise = Promise.resolve({json: () => mockJsonPromise, status, text: jest.fn()});
    // @ts-ignore
    jest.spyOn(global, "fetch").mockImplementationOnce(() => mockFetchPromise);
}

function thenExpectFetchCalledWith(expectedJson: string) {
    expect(dispatch).toHaveBeenCalledWith(addDvdPending());
    // @ts-ignore
    expect(global.fetch).toHaveBeenCalledWith("http://test.unit/adddvd/dvd/", {
        body: expectedJson,
        headers: new Headers({
            "Authorization": "Bearer AAAA-BB-CC-DDDD",
            "Content-Type": "application/json;charset=UTF-8",
        }),
        method: "put",
    });
}
