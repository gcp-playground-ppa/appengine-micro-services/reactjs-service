import {ThunkAction, ThunkDispatch} from "redux-thunk";
import {RootState} from "../store/index";
import {Action} from "redux";
import {Dvd} from "../dto/Dvd";
import {addDvdFail, addDvdPending, addDvdSuccess} from "./AppActions";

type ThunkResult<R> = ThunkAction<R, RootState, undefined, Action>;

function addDvd(dvd: Dvd): ThunkResult<void> {

    return (dispatch: ThunkDispatch<RootState, undefined, Action>, getState: () => RootState) => {
        const bearer = getState().services.bearer;
        const options = {
            body: JSON.stringify(dvd),
            headers: new Headers({
                "Authorization": `Bearer ${bearer}`,
                "Content-Type": "application/json;charset=UTF-8",
            }),
            method: "put",
        };

        dispatch(addDvdPending());
        fetch(`${process.env.REACT_APP_SERVICE_DVDSTORE}dvd/`, options)
            .then((response) => {
                if (response.status == 409) {
                    return Promise.reject<string>(`Dvd déjà ajouté`);
                } else if (response.status != 201) {
                    return Promise.reject<string>(`fail to add DVD, status ${response.status}`);
                } else {
                    return response.json();
                }
            })
            .then((dvds) => {
                dispatch(addDvdSuccess(dvds));
            }, (reason) => {
                dispatch(addDvdFail(reason));
            });
    };
}

export default addDvd;