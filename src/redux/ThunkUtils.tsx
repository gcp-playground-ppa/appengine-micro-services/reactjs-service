export const GET_JSON = {
    headers: new Headers({
        "Content-Type": "application/json;charset=UTF-8",
    }),
    method: "get",
};
