import React from 'react';
import {shallow, ShallowWrapper} from "enzyme";
import {ServicesComponent} from '../Services';
import MicroservicesLoadingView from '../../component/MicroservicesLoadingView';
import { Redirect } from 'react-router';

let bundle: { wrapper: ShallowWrapper<typeof ServicesComponent>, instance: typeof ServicesComponent };

describe("When Services did mount", () => {

    it("should display loading services", () => {
        givenView();

        expect(bundle.wrapper.find(MicroservicesLoadingView)).toHaveLength(1);
    });

    it("should redirect when services loaded", () => {
        givenView({loading: false});

        expect(bundle.wrapper.find(Redirect).props().to).toEqual("/add");
    });
});

function givenView({loading = true} = {}) {
    let wrapper = shallow(<ServicesComponent loading={loading}/>);
    let instance = wrapper.instance() as unknown as typeof ServicesComponent;
    bundle = {wrapper, instance};
}

