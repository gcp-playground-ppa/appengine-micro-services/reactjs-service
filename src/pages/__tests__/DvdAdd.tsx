import React from 'react';
import {shallow, ShallowWrapper} from "enzyme";
import {DvdAddComponent} from "../DvdAdd";
import Button from "react-bootstrap/Button";
import FormControl from "react-bootstrap/FormControl";
import {Dvd} from "../../redux/dto/Dvd";
import ModalList from "../../component/ModalList";
import {Redirect} from "react-router";
import Spinner from "react-bootstrap/Spinner";
import CardColumns from "react-bootstrap/CardColumns";
import DvdCard from "../../component/DvdCard";
import AlertDismissible from "../../component/AlertDismissible";
import ModalScanner from "../../component/ModalScanner";

const searchDvdAction = jest.fn();
const displayScannerAction = jest.fn();
const setEanAction = jest.fn();
const fetchDvdAction = jest.fn();
let bundle: { wrapper: ShallowWrapper<typeof DvdAddComponent>, instance: typeof DvdAddComponent };

beforeEach(() => {
    searchDvdAction.mockClear();
    setEanAction.mockClear();
    fetchDvdAction.mockClear();
    displayScannerAction.mockClear();
});

describe("DvdAdd action", () => {
    let useEffect: any;

    beforeEach(() => {
        /* mocking useEffect */
        useEffect = jest.spyOn(React, "useEffect").mockImplementation((f: any) => f());
    });

    afterEach(() => {
        useEffect.mockRestore();
    });

    it("Should fetch User's DVD", () => {

        givenView();

        expect(fetchDvdAction).toHaveBeenCalled();
    });

    it("Should display user's dvd", () => {
        givenView({dvds: [new Dvd("123")]});

        expect(bundle.wrapper.find(DvdCard)).toHaveLength(1);
    });

    it("Should display spinner while waiting", () => {
        givenView({waiting: true});

        expect(bundle.wrapper.find(Spinner)).toHaveLength(1);
        expect(bundle.wrapper.find(CardColumns)).toHaveLength(0);
    });

    it("Should redirect to / when no bearer", () => {
        givenView({ean: "123", bearer: ""});

        expect(bundle.wrapper.find(Redirect)).toHaveLength(1);
    });

    it("input value muse be props value", () => {
        givenView({ean: "123"});

        expect(bundle.wrapper.find(FormControl).last().props().value).toEqual("123");
    });

    it("call setEan Action on search field change", () => {
        givenView({ean: "123"});

        whenEanTyped("456");

        expect(setEanAction).toHaveBeenCalledWith("456");
    });

    it("display modal when result given", () => {
        givenView({ean: "123", searchResult: [new Dvd("zeTitle")]});

        expect(bundle.wrapper.find(ModalList)).toHaveLength(1);
    });

    it("search DVD from EAN", () => {
        givenView({ean: "456"});

        bundle.wrapper.find(Button).first().simulate("click");

        expect(searchDvdAction).toHaveBeenCalledWith("456");
    });

    it("Should display message", () => {
        givenView({message: "zeMessage"});

        expect(bundle.wrapper.find(AlertDismissible)).toHaveLength(1);
    });

    it("Should display modal scanner on click Camera button", () => {
        givenView();

        bundle.wrapper.find(Button).at(1).simulate("click");

        expect(displayScannerAction).toHaveBeenCalled();
        //expect(bundle.wrapper.find(ModalScanner)).toHaveLength(1);
    });

});

function givenView({
                       ean = "",
                       searchResult = [] as unknown as Dvd[],
                       dvds = [] as unknown as Dvd[],
                       bearer = "123",
                       message = "",
                       waiting = false,
                   } = {}) {
    let wrapper = shallow(
        (
            <DvdAddComponent
                bearer={bearer}
                searchEan={searchDvdAction}
                fetchDvds={fetchDvdAction}
                displayScanner={displayScannerAction}
                message={message}
                setEan={setEanAction}
                ean={ean}
                searchResult={searchResult}
                dvds={dvds}
                waiting={waiting}
            />
        )
    );
    let instance = wrapper.instance() as unknown as typeof DvdAddComponent;
    bundle = {wrapper, instance};
}

function whenEanTyped(ean: string) {
    const event = {
        target: {value: ean},
    };
    bundle.wrapper.find(FormControl).last().simulate("change", event);
}
