import React from 'react';
import {shallow, ShallowWrapper} from "enzyme";
import {LoginComponent} from "../Login";
import {mapStateToProps} from "../Login.redux";
import {Redirect} from "react-router";

let bundle: { wrapper: ShallowWrapper<typeof LoginComponent>, instance: typeof LoginComponent };
const retrieveToken = jest.fn();
const code = "5498d1e8-a9a8-4c63-9e07-96b60bfb2a3d.7b43f302-7aa1-4008-8f7e-bfbbc0784848.b88ce206-63d6-43b6-87c9-ea09d8c02f32";

describe("When Login start", () => {
    const {location} = window;

    beforeEach(() => {
        retrieveToken.mockClear();
        delete window.location;
        // @ts-ignore
        window.location = {
            reload: jest.fn(),
            href: `/localhost/?session_state=7b43f302-7aa1-4008-8f7e-bfbbc0784848&code=${code}`
        };
    });

    afterAll(() => {
        window.location = location;
    });

    it("should retrieve bearer token", () => {
        givenView();

        expect(retrieveToken).toHaveBeenCalledWith(code);
    });

    it("should redirect when bearer provided", () => {
        givenView("ABCD");

        expect(retrieveToken).not.toHaveBeenCalledWith(code);
        expect(bundle.wrapper.find(Redirect).props().to).toEqual("/services");
    });

    it("should map bearer to props", () => {
        // @ts-ignore
        const props = mapStateToProps({services: {bearer: "ABC", all: new Map()}});

        expect(props).toEqual({bearer: "ABC"});
    });
});

function givenView(bearer = "") {
    let wrapper = shallow(<LoginComponent retrieveToken={retrieveToken} bearer={bearer}/>);
    let instance = wrapper.instance() as unknown as typeof LoginComponent;
    bundle = {wrapper, instance};
}
