import {setError} from "../redux/actions/AppActions";

export interface IDispatchProps {
    setError: (message: string) => void;
}

export const mapDispatchToProps: IDispatchProps = {
    setError: setError,
}
