import {Dvd} from "../redux/dto/Dvd";
import deleteDvd from "../redux/actions/deleteDvdThunkAction";

export interface IDispatchProps {
    onDelete: (dvd: Dvd) => void;
}

export const mapDispatchToProps: IDispatchProps = {
    onDelete: deleteDvd
};