import React from 'react';
import {shallow, ShallowWrapper} from "enzyme";
import WelcomeView from "../WelcomeView";
import Button from "react-bootstrap/Button";

let bundle: { wrapper: ShallowWrapper<typeof WelcomeView>, instance: typeof WelcomeView };

describe("Welcome component", () => {
    const {location} = window;

    beforeEach(() => {
        delete window.location;
        // @ts-ignore
        window.location = {
            reload: jest.fn(),
            href: `/localhost/`
        };
    });

    afterAll(() => {
        window.location = location;
    });

    it("should redirect to keycloak login when click on button ", () => {
        givenView();

        bundle.wrapper.find(Button).simulate('click');

        expect(window.location.href).toEqual(`http://test.unit/oauth/authorize?response_type=code&&scope=write%20read&client_id=newClient&redirect_uri=http://site.url/login/`);
    });
});

function givenView() {
    let wrapper = shallow(<WelcomeView/>);
    let instance = wrapper.instance() as unknown as typeof WelcomeView;
    bundle = {wrapper, instance};
}
