import {shallow, ShallowWrapper} from "enzyme";
import React from "react";
import {AlertDismissibleComponent} from "../AlertDismissible";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";

const setError = jest.fn();
let bundle: { wrapper: ShallowWrapper<typeof AlertDismissibleComponent>, instance: typeof AlertDismissibleComponent };

describe("DVD Card", () => {

    it("should display a DVD", () => {
        givenAlert("zeMessage");

        expect(bundle.wrapper.find(Alert)).toHaveLength(1);
    });

    it("should display a DVD", () => {
        givenAlert("zeMessage");

        bundle.wrapper.find(Button).simulate("click");

        expect(setError).toHaveBeenCalled();
    });
});

function givenAlert(message: string) {
    let wrapper = shallow(<AlertDismissibleComponent message={message} setError={setError}/>);
    let instance = wrapper.instance() as unknown as typeof AlertDismissibleComponent;
    bundle = {wrapper, instance};
}
