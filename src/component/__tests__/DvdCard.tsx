import React from "react";
import {Dvd} from "../../redux/dto/Dvd";
import {shallow, ShallowWrapper} from "enzyme";
import {DvdCardComponent} from "../DvdCard";
import Card from "react-bootstrap/Card";
import {Fab} from "@material-ui/core";
import {Link} from "react-router-dom";

const deleteDvdAction = jest.fn();
let bundle: { wrapper: ShallowWrapper<typeof DvdCardComponent>, instance: typeof DvdCardComponent };

beforeEach(() => {
    deleteDvdAction.mockClear();
});

describe("DVD Card", () => {

    it("should display a DVD", () => {
        givenCard(new Dvd("123"));

        expect(bundle.wrapper.find(Card)).toHaveLength(1);
    });
});

function givenCard(dvd: Dvd) {
    let wrapper = shallow(<DvdCardComponent dvd={dvd} onDelete={deleteDvdAction}/>);
    let instance = wrapper.instance() as unknown as typeof DvdCardComponent;
    bundle = {wrapper, instance};
}
