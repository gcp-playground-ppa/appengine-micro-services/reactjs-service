import React from "react";
import {shallow, ShallowWrapper} from "enzyme";
import {ModalScannerComponent} from "../ModalScanner";
import Modal from "react-bootstrap/Modal";
import BarcodeScannerComponent from "react-webcam-barcode-scanner";
import {Result} from "@zxing/library";

let bundle: { wrapper: ShallowWrapper<typeof ModalScannerComponent>, instance: typeof ModalScannerComponent };

const searchDvdAction = jest.fn();
const closeAction = jest.fn();

beforeEach(() => {
    closeAction.mockClear();
    searchDvdAction.mockClear();
});

describe("Modal scanner", () => {

    it("can be open", () => {
        givenModalScanner({show: true});

        expect(bundle.wrapper.find(Modal).props().show).toBeTruthy();
    });

    it("can be close", () => {
        givenModalScanner({show: false});

        expect(bundle.wrapper.find(Modal).props().show).toBeFalsy();
    });

    it("can call close", () => {
        givenModalScanner({show: true});

        bundle.wrapper.find(Modal).simulate("hide");

        expect(closeAction).toHaveBeenCalled();
    });

    it("can scan ean and search", () => {
        givenModalScanner({show: true});

        // @ts-ignore
        const myResult: Result = new Result("123",null,0,[],null);
        bundle.wrapper.find(BarcodeScannerComponent).simulate("update", "", myResult);

        expect(searchDvdAction).toHaveBeenCalledWith("123");
    });
});

function givenModalScanner({show}: { show: boolean }) {
    let wrapper = shallow(<ModalScannerComponent show={show} close={closeAction} searchDvd={searchDvdAction}/>);
    let instance = wrapper.instance() as unknown as typeof ModalScannerComponent;
    bundle = {wrapper, instance};
}