import React from "react";
import {Dvd} from "../../redux/dto/Dvd";
import {shallow, ShallowWrapper} from "enzyme";
import DvdGridComponent from "../DvdGrid";
import DvdCard from "../DvdCard";

let bundle: { wrapper: ShallowWrapper<typeof DvdGridComponent>, instance: typeof DvdGridComponent };

beforeEach(() => {
});

describe("DVD Grid", () => {

    it("should display a DVD", () => {
        givenCard([new Dvd("123")]);

        expect(bundle.wrapper.children()).toHaveLength(1);
    });

    it("should a DVD row", () => {
        givenCard(Array.from(Array(3).keys()).map(() => new Dvd("123")));

        expect(bundle.wrapper.children()).toHaveLength(3);
    });

    it("should 4 dvds with a full row", () => {
        givenCard(Array.from(Array(4).keys()).map(() => new Dvd("123")));

        expect(bundle.wrapper.children()).toHaveLength(5);
    });
});

function givenCard(dvds: Dvd[]) {

    const cards = dvds.map((dvd: Dvd, index: number) => {
        return <DvdCard dvd={dvd} key={index}/>;
    });
    let wrapper = shallow((
        <DvdGridComponent>
            {cards}
        </DvdGridComponent>
    ));
    let instance = wrapper.instance() as unknown as typeof DvdGridComponent;
    bundle = {wrapper, instance};
}
