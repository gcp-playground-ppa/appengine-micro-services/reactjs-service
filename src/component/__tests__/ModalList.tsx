import {shallow, ShallowWrapper} from "enzyme";
import React from "react";
import {ModalListComponent} from "../ModalList";
import {Dvd} from "../../redux/dto/Dvd";
import ListGroup from "react-bootstrap/ListGroup";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

const addDvdAction = jest.fn();
const cancelAction = jest.fn();
let bundle: { wrapper: ShallowWrapper<typeof ModalListComponent>, instance: typeof ModalListComponent };

beforeEach(() => {
    addDvdAction.mockClear();
    cancelAction.mockClear();
});

describe("DVD Modal liste", () => {

    it("should display a list of DVDs", () => {
        givenList([new Dvd("123")]);

        expect(bundle.wrapper.find(ListGroup.Item)).toHaveLength(1);
    });

    it("should cancel add DVD", () => {
        givenList([new Dvd("123")]);

        bundle.wrapper.find(Modal).simulate("hide");

        expect(cancelAction).toHaveBeenCalled();
    });

    it("should validate on button action", () => {
        givenList([new Dvd("123"), new Dvd("456")]);

        bundle.wrapper.find(ListGroup.Item).last().simulate("click");
        bundle.wrapper.find(Button).simulate("click");

        expect(addDvdAction).toHaveBeenCalledWith(new Dvd("456"));
    });
});

function givenList(dvds = [] as unknown as Dvd[]) {
    let wrapper = shallow(<ModalListComponent dvds={dvds} onSave={addDvdAction} onCancel={cancelAction}/>);
    let instance = wrapper.instance() as unknown as typeof ModalListComponent;
    bundle = {wrapper, instance};
}
