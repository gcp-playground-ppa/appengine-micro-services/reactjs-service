import React from 'react';
import {EnzymePropSelector, shallow, ShallowWrapper} from 'enzyme';
import MicroservicesLoadingView from "../MicroservicesLoadingView";
import MicroserviceLoadingView from "../MicroserviceLoadingView";

let bundle: { wrapper: ShallowWrapper<typeof MicroservicesLoadingView>, instance: typeof MicroservicesLoadingView };

describe("When MicroservicesLoadingView mount", () => {

    it("CircularProgress should be displayed", () => {
        givenLoadings();

        thenShouldDisplay(MicroserviceLoadingView);
    });
});

function givenLoadings() {
    let wrapper = shallow(<MicroservicesLoadingView/>);
    let instance = wrapper.instance() as unknown as typeof MicroservicesLoadingView;
    bundle = {wrapper, instance};
}

function thenShouldDisplay(expected: EnzymePropSelector) {
    expect(bundle.wrapper.find(expected).length).not.toBe(0);
}
