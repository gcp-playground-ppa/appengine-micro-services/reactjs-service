import React from 'react';
import {EnzymePropSelector, mount, ReactWrapper} from 'enzyme';
import {IMicroserviceLoadingViewProps, MicroserviceLoadingViewComponent} from "../MicroserviceLoadingView";
import {CircularProgress} from "@material-ui/core";
import {RootState} from "../../redux/store/index";
import {initialServicesState} from "../../redux/types/ServicesState";
import {mapStateToProps} from "../MicroserviceLoadingView.redux";

let bundle: {
    wrapper: ReactWrapper<typeof MicroserviceLoadingViewComponent>,
    instance: typeof MicroserviceLoadingViewComponent
};
let wakeUpService: jest.Mock<any, any> = jest.fn();

beforeEach(() => {
    wakeUpService.mockClear();
});

describe("When MicroservicesLoadingView mount", () => {

    it("CircularProgress should be displayed", (done) => {
        givenLoadings("Test service", "0");

        thenShouldDisplayOnce(CircularProgress);
        expect(bundle.wrapper.find(CircularProgress).props().variant).toEqual("indeterminate");
        expect(bundle.wrapper.find(CircularProgress).props().color).toEqual("primary");
        expect(bundle.wrapper.find(".serviceMessage").first().text()).toEqual("Chargement Test service ...");

        process.nextTick(() => {
            expect(wakeUpService).toHaveBeenCalledWith("Test service")
            done();
        });
    });

    it("CircularProgress should be Up", () => {
        givenLoadings("Test service", "1");

        thenShouldDisplayOnce(CircularProgress);
        expect(bundle.wrapper.find(CircularProgress).props().variant).toEqual("determinate");
        expect(bundle.wrapper.find(CircularProgress).props().color).toEqual("inherit");
        expect(bundle.wrapper.find(".serviceMessage").first().text()).toEqual("Test service prêt à fonctionner");
    });

    it("CircularProgress should be Down", () => {
        givenLoadings("Test service", "-1");

        thenShouldDisplayOnce(CircularProgress);
        expect(bundle.wrapper.find(CircularProgress).props().variant).toEqual("determinate");
        expect(bundle.wrapper.find(CircularProgress).props().color).toEqual("secondary");
        expect(bundle.wrapper.find(".serviceMessage").first().text()).toEqual("Test service ne répond pas");
    });
    
    it("should map existing service from RootState", () => {
        // @ts-ignore
        const state: RootState = {services: initialServicesState};
        const props: IMicroserviceLoadingViewProps = {name: "Bibliothèque Dvds"};
        const serviceStatus = mapStateToProps(state, props);
        expect(serviceStatus).toEqual({serviceState: "0"});
    });

    it("should map unknown service to failing", () => {
        const state: RootState = {services: initialServicesState};
        const props: IMicroserviceLoadingViewProps = {name: "unknown"};
        const serviceStatus = mapStateToProps(state, props);
        expect(serviceStatus).toEqual({serviceState: "-1"});
    });
});

function givenLoadings(serviceLibelle: string, serviceState: string) {
    let wrapper = mount((
        <MicroserviceLoadingViewComponent
            name={serviceLibelle}
            wakeAction={wakeUpService}
            serviceState={serviceState}
        />
    ));
    let instance = wrapper.instance() as unknown as typeof MicroserviceLoadingViewComponent;
    bundle = {wrapper, instance};
}

function thenShouldDisplayOnce(expected: EnzymePropSelector) {
    expect(bundle.wrapper.find(expected)).toHaveLength(1);
}
