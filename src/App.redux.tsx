import { RootState } from "./redux/store/index";
import wakeServiceThunkAction from "./redux/actions/wakeServiceThunkAction";

export interface IDispatchProps {
    wake: (name: string) => void;
}

export const mapDispatchToProps: IDispatchProps = {
    wake: wakeServiceThunkAction,
};